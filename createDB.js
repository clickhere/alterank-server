const config = require('./config');
var mysql = require('mysql');

var DBDetails = {
	host: config.db.host,
	user: config.db.user,
	password: config.db.password
};

var DB = mysql.createConnection(DBDetails);

var createDB = function(DBName) {
	DB.query("CREATE DATABASE IF NOT EXISTS " + DBName, function (err, response) {
		if (err)
			throw err;
		console.log("Connected, DB Created");
	});
};

var createTable = function(tableName, tableFields) {
	DB.query("CREATE TABLE " + tableName + " " + tableFields, function(err, response) {
		if (err)
			if (err.errno === 1050)
				console.log("Table '" + tableName + "' already exists");
			else
				throw err;
		else
			console.log("Table '" + tableName + "' created");
	});
};

createDB(config.db.database);
var DB = mysql.createConnection(config.db);

var usersFields = "(id int NOT NULL AUTO_INCREMENT PRIMARY KEY, " + 
	"firstname VARCHAR(255) CHARACTER SET utf8, " + 
	"lastname VARCHAR(255) CHARACTER SET utf8, " + 
	"email VARCHAR(255) CHARACTER SET utf8, " + 
	"phone VARCHAR(255) CHARACTER SET utf8, " +
	"password VARCHAR(255) CHARACTER SET utf8, " +
	"emailconfirmed TINYINT(1) DEFAULT 0)";

createTable('users', usersFields);