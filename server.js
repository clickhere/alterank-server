var restify = require('restify');
var server = restify.createServer();
var corsMiddleware = require('restify-cors-middleware');
var mysql = require('mysql');
var nodemailer = require('nodemailer');
var passport = require('passport');
var BearerStrategy = require('passport-http-bearer').Strategy;
const config = require('./config');

var cors = corsMiddleware({
	origins: ['*']
});

var DB = mysql.createConnection(config.db);

server.pre(cors.preflight);
server.use(cors.actual);
server.use(restify.plugins.bodyParser());
server.use(passport.initialize());

passport.use(new BearerStrategy(
	function(token, done) {
		User.findOne({ token: token }, function (err, user) {
      if (err) { return done(err); }
      if (!user) { return done(null, false); }
      return done(null, user, { scope: 'all' });
    });
	}
));

passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(user, done) {
  done(null, user);
});

const transporter = nodemailer.createTransport({
  host: 'smtp.ethereal.email',
  port: 587,
  secure: false,
  auth: {
    user: 'szsgx42jc6zrwo6a@ethereal.email',
    pass: 'QHTGXKGCv9ye4Z4Y1V'
  }
});


var sendConfirmationEmail = (email, token) => {
	
	let mailOptions = {
	  from: '"AlteRank" <szsgx42jc6zrwo6a@ethereal.email>',
	  to: email,
	  subject: 'Confirmation Email',
	  text: `Please confirm your registration`,
	  html: `<span>Please confirm your registration by clicking <a href="http://localhost:3000/${token}">Here</a></span>`
	};

	transporter.sendMail(mailOptions, (error, info) => {
	  if (error) {
	      return console.log(error);
	  }
	  console.log('Message sent: %s', info.messageId);
	  console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
	});

}



var isLoggedIn = function(request, response, next) {

	next();
};

var performQuery = function(queryString, parameters) {
	DB.query(queryString, parameters, function(err, results) {
		if (err) {
			return true;
		}
	});
};

var insertToTable = function (tableName, fields, values) {
	var queryString = "INSERT INTO ?? (??) VALUES (?)";
	var err = performQuery(queryString, [tableName, fields, values]);
	if (err) {
		return true;
	}
};

var extractFields = function(entryObject) {
	return Object.keys(entryObject);
};

var extractValues = function(entryObject) {
	return Object.values(entryObject);
};

const signup = (request, response, next) => {
	var signupDetails = request.body.signupDetails,
		email = signupDetails.email,
		token = 'AAAAA',
		fields = extractFields(signupDetails),
		values = extractValues(signupDetails);
	if (response.statusCode === 200) {
		insertToTable('users', fields, values);
		sendConfirmationEmail(email, token);
		response.send("Success");
	} else {
		response.send("Error");
	}
	next();
}

const signin = (request, response, next) => {
	const userDetails = request.body.signinDetails;
	next();
}

const signout = (request, response, next) => {
	return true;
}

server.get('/', (request, response, next) => {
	if (true) {
		 return response.send("0");
	}
	return response.send("1");
});

server.post('/signup', signup);

server.post('/signin', signin);

server.post('/signout', signout);

server.listen(8080, function() {
	console.log('Listening on 8080');
});